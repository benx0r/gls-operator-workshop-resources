#!/usr/bin/env bash
#
# This script configures the workstation in a DO280 classroom for use in
# the Openshift Operator workshop

# install Ansible kubernetes modules with ansible galaxy
#ansible-galaxy collection install community.kubernetes

# install python modules
### XXX yum pulls in python 3.6 packages XXX
#sudo yum -y install python3-pyyaml python3-openshift python3-kubernetes
# XXX OLD NOT AVAILABLE ON RHEL9 XXX
#sudo yum -y install python38
#sudo alternatives --set python3 /usr/bin/python3.8
#
#sudo pip3 install --upgrade pip
#sudo pip3 install pyyaml openshift kubernetes
# XXX END NOT AVAILABLE ON RHEL9 XXX

# clean up
rm -rf ~/.ansible

# do something else

# install make
sudo yum -y install make python3-kubernetes

