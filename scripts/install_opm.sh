#!/usr/bin/env bash
#
# Download and install OPM.
#
# Set the download variables explicitly.
SYSTEM=linux

# OpenShift version is different - this should be working by now, if steps were followed.
OPENSHIFT_VERSION="$(oc whoami 2>/dev/null)"
RV="$?"
if [ ${RV} -ne 0 ]; then
    echo "ERROR: Not logged into OpenShift. Attempting to log in."
    if [ ! -e /usr/local/etc/ocp4.config ]; then
        echo "FATAL: Can not log in. Follow the instructions in the textbook before running this script." >&2
        exit 1
    fi

    source /usr/local/etc/ocp4.config
    oc login -u kubeadmin -p ${RHT_OCP4_KUBEADM_PASSWD} ${RHT_OCP4_MASTER_API} >/dev/null 2>&1
    RV="$?"

    if [ ${RV} -ne 0 ]; then
        echo "FATAL: OpenShift autologin failed. Follow the instructions in the textbook before running this script." >&2
        exit 1
    fi
fi
OPENSHIFT_VERSION="$(oc version -o json | jq -r .openshiftVersion)"

echo "Downloading OPM ${OPENSHIFT_VERSION} for ${SYSTEM}..."

# Download the matching version from mirror.openshift.com.
curl -O https://mirror.openshift.com/pub/openshift-v4/clients/ocp/${OPENSHIFT_VERSION}/opm-${SYSTEM}-${OPENSHIFT_VERSION}.tar.gz

# Checksums.
curl https://mirror.openshift.com/pub/openshift-v4/clients/ocp/${OPENSHIFT_VERSION}/sha256sum.txt | \
    grep opm-${SYSTEM}-${OPENSHIFT_VERSION}.tar.gz > sha256sum.txt
sha256sum -c sha256sum.txt && echo "Downloads OK, deploying..." || (echo "Error downloading, aborting." && exit 1)
rm -f sha256sum.txt

# Extract and install.
tar xf opm-${SYSTEM}-${OPENSHIFT_VERSION}.tar.gz opm
sudo cp opm /usr/local/bin/opm
sudo chmod 755 /usr/local/bin/opm
rm -f opm opm-${SYSTEM}-${OPENSHIFT_VERSION}.tar.gz

echo "Done!"

# End of install_opm.sh
